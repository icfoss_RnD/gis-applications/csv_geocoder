import geocoder
import csv
import sys
import time
import json
input_csv = sys.argv[1]
out_provider = sys.argv[2]
out = open("output/output_"+out_provider+"_"+input_csv.strip('.csv')+"_"+str(time.strftime("%s", time.gmtime()))+".geojson","w")
output_geojson = {"type": "FeatureCollection","features": []}
with open(input_csv) as input_file:
	reader = csv.reader(input_file)
	count = -1
	for row in reader:
		count+=1
		if count == 0:
			continue
		addr = ''
		for column in row:
			addr += column+', '
		addr=addr.strip(", ")
		if out_provider == 'osm':
			g = geocoder.osm(addr)
		elif out_provider == 'arcgis':
			g = geocoder.arcgis(addr)
		elif out_provider == 'google':
			g = geocoder.google(addr)
		else:
			print 'ERROR!!!! ;)'
			break
		if len(g.geojson['features']) == 0:
			print 'Place '+str(count)+' Geocode not Found!!'
			continue
		print 'Writing place '+str(count)
		feature_json = {"geometry":g.geojson['features'][0]['geometry'],"type":g.geojson['features'][0]['type'],"properties":{"name":addr}}
		output_geojson['features'].append(feature_json)
print 'Final output: \n'
print(json.dumps(output_geojson))
out.write(json.dumps(output_geojson))
out.close()
